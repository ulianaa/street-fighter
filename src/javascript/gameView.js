import View from './view'
import Fighter from './fighter'

class GameView extends View {
    constructor(fightersDetailsMap) {
        super();
        this.fightersDetailsMap = fightersDetailsMap;
        this.createGameField();
    }

    createGameField() {
        this.element = this.createElement({ tagName: 'div', className: 'gameField' });
        const button = this.createButton();
        this.status = false;
        button.innerText = 'start game';
        this.element.appendChild(button);
        this.p = this.createElement({ tagName: 'p', className: 'status' });
        this.p.innerText = '';
        this.element.appendChild(this.p);

    }

    createButton() {
        const button = this.createElement({ tagName: 'button', className: 'gameButton' });
        button.addEventListener('click', this.handleButtonClick);
        return button;
    }

    fight(attack, defense) {
        const damage = attack.getHitPower() - defense.getBlockPower();
        return (damage < 0) ? 0 : damage;
    }

    getSelectedFighters() {
        this.fighters = Array.from(this.fightersDetailsMap.values())
        .filter(el => el.checked)
        .map(el => new Fighter(el));  
    }

    swapFighters() { 
        this.fighters = [this.fighters[1], this.fighters[0]];
    }

    startGame() {
        this.status = true;
        console.log('start game');
        if (Math.random() - 0.5 > 0) {
          this.swapFighters();
        }    
        this.interval = setInterval(this.nextTurn, 1000);
    }
    

    stopGame() {
        this.status = false;
        clearInterval(this.interval);
    }

    changeFighter() {
        this.swapFighters();
        const damage = this.fight(this.fighters[0], this.fighters[1]);
        this.fighters[1].health -= damage;
        if (this.fighters[1].health <= 0) {
          this.stopGame();
        }
      };

    nextTurn = () => {
        this.swapFighters();
    
        const damage = this.fight(this.fighters[0], this.fighters[1]);
        this.fighters[1].health -= damage;
        
        if (this.fighters[1].health <= 0) {
          this.stopGame();
          this.p.innerText = `The winner is 
          ${this.fighters[0].name}`;
        }
    };

    handleButtonClick = () => {
        if (this.status == false) {
            this.getSelectedFighters();
            if (this.fighters.length == 2) {
                this.startGame();
            }
            else (this.p.innerText = `Please choose 
            2 fighters`);
        }
    }
}

export default GameView;