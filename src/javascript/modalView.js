import View from './view';
import FightersView from './fightersView';

class ModalView extends View {
    constructor(fighterDetails) {
        super();
        
        this.createModal(fighterDetails);
        this.content = fighterDetails;
    }
    
    content;

    createModal(fighterDetails) {
        this.element = this.createElement({ tagName: 'div', className: 'overlay' });
        const modal = this.createElement({ tagName: 'div', className: 'modal' });
        const form = this.createForm(fighterDetails);
        
        this.element.appendChild(modal);
        modal.appendChild(form);
        
        this.element.addEventListener('click', event => {
            if (event.target.className === 'overlay') {
                event.target.remove();
            }
        }, false);
    }

    createForm(fighterDetails) {
        const attributes = {};
        const form = this.createElement({ tagName: 'form', className: 'information', attributes}); 
        const {id, name, health, attack, defense, source} = fighterDetails;
        const p = this.createElement({ tagName: 'p', className: 'name'});
        p.innerText = name;
        form.appendChild(p);
        form.appendChild(this.createContent('health', health));
        form.appendChild(this.createContent('attack', attack));
        form.appendChild(this.createContent('defense', defense));
        const submit = this.createElement({ tagName: 'div', className: 'submit'});
        submit.innerText = 'Edit information';
        submit.addEventListener('click', () => {this.changeContent(fighterDetails);}, false);
        form.appendChild(submit);

        return form;
    }

    createContent(key, content) {
        const field = this.createElement({ tagName: 'p', className: 'element' });
        const input = this.createElement({ tagName: 'input', className: 'data', 
        attributes: {type: 'number', value: `${content}`, id: `${key}`, min: 0}});
        field.innerText = `${key}   `;
        field.appendChild(input);
        return field;
    }

    changeContent(fighterDetails) {
        const inputs = this.element.querySelectorAll('input[type=number]');
        for (let i = 0; i < inputs.length; i++) {
            for (let key in fighterDetails) {
                if (inputs[i].id == key) {
                    this.content[key] = Math.abs(+inputs[i].value);
                }
            }
        }
        
        console.log(this.content);
        this.element.remove();
        console.log('success');
        return this.content;
    }
}

export default ModalView;