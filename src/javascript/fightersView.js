import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';
import ModalView from './modalView';
import GameView from './gameView';

class FightersView extends View {
  constructor(fighters) {
    super();
    
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
    this.createGame();
  }

  fightersDetailsMap = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  createGame() {
    const gameField = new GameView(this.fightersDetailsMap);
    this.element.appendChild(gameField.element);
  }

  async handleFighterClick(event, fighter) {
    if (!this.fightersDetailsMap.has(fighter._id)) {
      const fighterDetails = await fighterService.getFighterDetails(fighter._id);
      this.fightersDetailsMap.set(fighter._id, fighterDetails);
    }
    if (event.target.tagName == 'INPUT') {
      this.fightersDetailsMap.get(fighter._id).checked = event.target.checked;
      return;
    }
    const modal = new ModalView(this.fightersDetailsMap.get(fighter._id));
    document.body.appendChild(modal.element);
  } 
}

export default FightersView;