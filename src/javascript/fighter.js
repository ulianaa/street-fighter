class Fighter {
    constructor(fighterDetails) {
        this.name = fighterDetails.name;
        this.health = fighterDetails.health;
        this.attack = fighterDetails.attack;
        this.defense = fighterDetails.defense;
    }

    getHitPower() {
        const criticalHitChance = Math.random() + 1;
        const power = Math.round(criticalHitChance * this.attack);
        return power;
    }

    getBlockPower() {
        const dodgeChance = Math.random() + 1;
        const power = Math.round(dodgeChance * this.defense);
        return power;
    }
}

export default Fighter;